<? require($_SERVER["DOCUMENT_ROOT"]."/boom-autogeocode/php/config.php"); ?>
<?=$SCAFFOLD_HEAD ?>
<main class="wrapper">
<?
/*******
* 
*   GeoLocation script - by Jan Baykara
*   Powered by cynicism and hayfever
* 
*******/

class geocoder{
    static private $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&region=uk&address=";

    static public function getLocation($address){
        $url = self::$url.urlencode($address);
        $resp_json = file_get_contents($url);
        
        if($resp['status']='OK'){
          $results['status'] = "ok";
          $results['json'] = $resp_json;
          return $results;
        } else {
          $results['status'] = "error";
        }
    }
}

if($_POST) {
  $form = $_POST;
  
  if ( isset($_FILES["file"])) {
      echo "<h3>CSV received</h3>";
      //if there was an error uploading the file
      if ($_FILES["file"]["error"] > 0) {
          echo "Error uploading/reading file: " . $_FILES["file"]["error"] . "<br />";
      }
      else {
        echo "<h3>Uploaded: " . $_FILES["file"]["name"] . "</h3>";
        /*
          echo "Type: " . $_FILES["file"]["type"] . "<br />";
          echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
          echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";
        */
          //Store file in directory "upload" with the name of "uploaded_file.txt"
          $storagename = "upload.csv";
          $csvlocation = "uploads/".$storagename;
          move_uploaded_file($_FILES["file"]["tmp_name"], $csvlocation);
          //echo "Stored in: " . $csvlocation . "<br />";
        
          $csvfile = fopen($csvlocation, "r");
          $z = 0;
          $csvarr;
        
          while(!feof($csvfile)) {
            if($z == 0) {
              $csvcols = fgetcsv($csvfile);
              //echo "<hr>";
              //print_r($csvcols);
            } else {
              $thisRow = fgetcsv($csvfile);
              //echo "<hr>";
              //print_r($thisRow);
              $csvrows[$z-1] = $thisRow;  
            }  
            $z++;
          }
        
          //echo "<hr>COLS";
          //print_r($csvcols);
          //echo "<hr>ROWS";
          //print_r($csvrows);
          //echo "<hr>ARR";
          foreach($csvrows as $key => $row) {
            $queries[$key] = array_combine($csvcols,$row);
          }
          //print_r($csvarr);
      }
    
   } else {
    echo "Inputs received";
    $query_searches = explode("\n", $form['query_search']);
    $query_ids = explode("\n", $form['query_id']);
    $queries = array_combine($query_ids,$query_searches);
  }
  
  if(!$form['template']) {
  echo "<h3>Results</h3><hr>";
  echo "<table>";
  echo "<tr>
          <th>Query ID</th>
          <th>Query Search</th>
          <th>Address</th>
          <th>Lat</th>
          <th>Lng</th>
        </tr>";
  }
  
  $s = 0;
  ob_start();
  foreach($queries as $id => $search) {
    
    $props = array(
      "id"      => $search['Name'],
      "search"   => $search['Address']
    );
    
    foreach($search as $key => $val) {
      $props['csv'][$key] = $val;
    }
    
    if(!$form['template']) {
      echo "<tr>
              <td>$props[id]</td>
              <td>$props[search]</td>";
    } else {
      echo "<br>";
    }
    
    // Get API data for $address
    $geoCodeData = geocoder::getLocation($props['search']);

    // Print retrieved data
    if($geoCodeData[status] == "ok") {
      
      $dataArr = json_decode($geoCodeData[json], true);
      $thisAddress = $dataArr[results][0];
      
      $props['address'] = $thisAddress['formatted_address'];
      $props['lat']     = $thisAddress['geometry']['location']['lat'];
      $props['lng']     = $thisAddress['geometry']['location']['lng'];
      
      if(!$form[template]) {
        echo "<td>$props[address]</td>";
        echo "<td>$props[lat]</td>";
        echo "<td>$props[lng]</td>";
      } else {
        // Output in data form.
        $output = $form[template];
        foreach($props as $prop => $val) {
          if($prop != "csv")
            $output = str_replace("@@".$prop, $val, $output);
        }
        foreach($props['csv'] as $CSVprop => $val) {
          if(!strpos($val,'@@@')) $val = '"'.$val.'"';
          $output = str_replace("@@csv_".$CSVprop, $val, $output);
        }
        echo $output;
      }
    
    // Or output error
    } else {
      if(!$form[template]) { echo "<td>Error retrieving data</td>"; }
    }

    // Pause if over-loading API data
    if(!$form[template]) { echo "</tr>"; }
    $s++;
    if($sleep = 9) {
      $s = 0;
      ob_flush();
      sleep(0.3);
    }
  }
  if(!$form[template]) { echo "</table>"; }
  
} else { ?>
  <form method='POST' enctype="multipart/form-data">
    <!--<textarea name='query_id' placeholder='Optional: input IDs for each address (e.g. copy/paste the NAME column of spreadsheet here)' rows=10 cols=50></textarea>
    <textarea name='query_search' placeholder='Each address on new line, no commas (copy/paste an excel column here)' rows=10 cols=50></textarea>-->
    <textarea name='template' value='{"title": "@@id",         "lat": @@lat, "lng": @@lng, "description": @@csv_Description, "link": @@csv_URL, "address": "@@address", "image": "img/jack.jpg"},'placeholder='Template to output data in. @@lat or @@lng etc. in template replaced by lat/lng/etc respectively. @@csv_XYZ echos value of column XYZ from CSV file.' rows=10 cols=50></textarea>
    <input type="file" name="file" value="Get lats/longs"/>
    <input type="submit" name="return" value="Get lats/longs"/>
    <input type="submit" name="return" value="Get full addresses"/>
    <input type="submit" name="return" value="Get raw JSON data"/>
    <input type="submit" name="return" value="Get CSV data"/>
  </form>
<? } ?>
</main>
<?=$SCAFFOLD_FOOT ?>