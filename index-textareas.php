<? require($_SERVER["DOCUMENT_ROOT"]."/boom-autogeocode/php/config.php"); ?>
<?=$SCAFFOLD_HEAD ?>
<main class="wrapper">
<?
/*******
* 
*   GeoLocation script - by Jan Baykara
*   Powered by cynicism and hayfever
* 
*******/

class geocoder{
    static private $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&region=uk&address=";

    static public function getLocation($address){
        $url = self::$url.urlencode($address);
        $resp_json = file_get_contents($url);
        
        if($resp['status']='OK'){
          $results['status'] = "ok";
          $results['json'] = $resp_json;
          return $results;
        } else {
          $results['status'] = "error";
        }
    }
}

if($_POST) {
  $form = $_POST;
  
  if(!$form['template']) {
  echo "<h3>Results</h3><hr>";
  echo "<table>";
  echo "<tr>
          <th>Query ID</th>
          <th>Query Search</th>
          <th>Address</th>
          <th>Lat</th>
          <th>Lng</th>
        </tr>";
  }
  
  $query_searches = explode("\n", $form['query_search']);
  $query_ids = explode("\n", $form['query_id']);
  $queries = array_combine($query_ids,$query_searches);
  
  $s = 0;
  ob_start();
  foreach($queries as $id => $search) {
    // Split address into two parts: 'ID' and 'SEARCH'
    
    $props = array(
      "id"      => $id,
      "query"   => $search
    );
    
    if(!$form['template']) {
      echo "<tr>
              <td>$props[id]</td>
              <td>$props[search]</td>";
    } else {
      echo "<br>";
    }
    
    // Get API data for $address
    $geoCodeData = geocoder::getLocation($query[search]);

    // Print retrieved data
    if($geoCodeData[status] == "ok") {
      
      $dataArr = json_decode($geoCodeData[json], true);
      $thisAddress = $dataArr[results][0];
      
      $props[address] = $thisAddress[formatted_address];
      $props[lat]     = $thisAddress[geometry][location][lat];
      $props[lng]     = $thisAddress[geometry][location][lng];
      
      if(!$form[template]) {
        echo "<td>$props[address]</td>";
        echo "<td>$props[lat]</td>";
        echo "<td>$props[lng]</td>";
      } else {
        // Output in data form.
        $output = $form[template];
        foreach($props as $prop => $val) {
          $output = str_replace("@@".$prop, $val, $output);
        }
        echo $output;
      }
    
    // Or output error
    } else {
      if(!$form[template]) { echo "<td>Error retrieving data</td>"; }
    }

    // Pause if over-loading API data
    if(!$form[template]) { echo "</tr>"; }
    $s++;
    if($sleep = 9) {
      $s = 0;
      ob_flush();
      sleep(0.3);
    }
  }
  if(!$form[template]) { echo "</table>"; }
  
} else { ?>
  <form method='POST'>
    <textarea name='query_id' placeholder='Optional: input IDs for each address (e.g. copy/paste the NAME column of spreadsheet here)' rows=10 cols=50></textarea>
    <textarea name='query_search' placeholder='Each address on new line, no commas (copy/paste an excel column here)' rows=10 cols=50></textarea>
    <textarea name='template' placeholder='[optional] template to output data in. @@LAT or @@LNG etc. in template replaced by lat/lng/etc respectively.' rows=10 cols=50></textarea>
    <input type="submit" name="return" value="Get lats/longs"/>
    <input type="submit" name="return" value="Get full addresses"/>
    <input type="submit" name="return" value="Get raw JSON data"/>
    <input type="submit" name="return" value="Get CSV data"/>
  </form>
<? } ?>
</main>
<?=$SCAFFOLD_FOOT ?>