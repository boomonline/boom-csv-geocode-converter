<? require($_SERVER["DOCUMENT_ROOT"]."/boom-autogeocode/php/config.php"); ?>
<?=$SCAFFOLD_HEAD ?>
<main class="wrapper">
<?
/*******
* 
*   GeoLocation script - by Jan Baykara
*   Powered by cynicism and hayfever
* 
*******/

class geocoder{
    static private $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";

    static public function getLocation($address){
        $url = self::$url.urlencode($address);
        
        $resp_json = file_get_contents($url);
        $resp = json_decode($resp_json, true);
        
        echo "<h3>Processing</h3>";
      
        echo "Seeking:<br>".$url."<hr>";
        
        if($resp['status']='OK'){
            echo "Data retrieved<hr>";
            switch($_GET['return']) {
              case "Get lats/longs":      
                echo "<h3>Latitudes and longitudes</h3><hr>";
                echo "<table>";
                echo "<tr>
                        <th>Input</th>
                        <th>Lat</th>
                        <th>Lng</th>
                      </tr>";
                foreach($resp['results'] as $thisLocation) {
                  echo "<tr>";
                  echo "<td>".$_GET['address'];
                  echo "<td>".$thisLocation['geometry']['location']['lat']."</td>";
                  echo "<td>".$thisLocation['geometry']['location']['lng']."</td>";
                  echo "</tr>";
                }
              echo "</table>";
              break;
              
              case "Get full addresses":  echo $resp_json; break;
              case "Get raw JSON data":   echo $resp_json; break;
              case "Get CSV data":        echo $resp_json; break;
            }
        } else {
            return "Meow";
        }
    }
}

if($_GET) {
  
  $address=urlencode($_GET['address']);
  $loc = geocoder::getLocation($address);
  
  /*
  switch($_GET['return']) {
    case "Get lats/longs":     break;
    case "Get full addresses": break;
    case "Get raw JSON data":  break;
    case "Get CSV data":       break;
  }
  */
  
  //print_r($loc);
  
} else { ?>
  <form method='GET'>
    Input file (CSV): <input type='text' name='address' />
    <input type="submit" name="return" value="Get lats/longs"/>
    <input type="submit" name="return" value="Get full addresses"/>
    <input type="submit" name="return" value="Get raw JSON data"/>
    <input type="submit" name="return" value="Get CSV data"/>
  </form>
<? } ?>
</main>
<?=$SCAFFOLD_FOOT ?>